import { createBrowserRouter, RouterProvider } from "react-router-dom";
import RootLayout from "./components/layouts/RootLayout";
import AddProduct from "./pages/AddProduct/AddProduct";
import CartPage from "./pages/CartPage/CartPage";
import ErrorPage from "./pages/ErrorPage";
import ShopPage from "./pages/ShopPage/ShopPage";

const router = createBrowserRouter([
  {
    path: "/",
    element: <RootLayout />,
    errorElement: <ErrorPage />,
    children: [
      { path: "/", element: <ShopPage /> },
      { path: "/add-product", element: <AddProduct /> },
      { path: "/cart-page", element: <CartPage /> },
    ],
  },
]);

function App() {
  return <RouterProvider router={router} />;
}

export default App;
