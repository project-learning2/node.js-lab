import { useState, useEffect } from "react";
import styles from "./CartPage.module.css";

const CartPage = () => {
  const [cart, setCart] = useState(null);
  const [dataProducts, setDataProducts] = useState(null);

  //   Có thể giảm số lượng lần call api tất cả sản phầm
  useEffect(() => {
    fetch("http://localhost:5000/get-cart")
      .then((response) => response.json())
      .then((data) => {
        setCart(data);
      })
      .catch((error) => console.error(error));
    fetch("http://localhost:5000/")
      .then((response) => response.json())
      .then((data) => setDataProducts(data))
      .catch((err) => console.error(err));
  }, []);

  const productsRender =
    cart && cart.products.length > 0
      ? cart.products.map((el) => (
          <div key={el.id}>
            <p>ID: {el.id}</p>
            <p>Qty: {el.qty}</p>
          </div>
        ))
      : "0 product in cart";

  return (
    <div className={styles["cart-page"]}>
      <h3>{productsRender}</h3>
      <h4>Total Price: {cart ? cart.totalPrice : "Loadingg......"}</h4>
    </div>
  );
};

export default CartPage;
