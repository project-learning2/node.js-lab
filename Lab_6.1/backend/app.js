const express = require("express");
const bodyParser = require("body-parser");

const cors = require("cors");
const app = express();

const users = [];

app.use(cors());
// app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.post("/add-user", (req, res, next) => {
  users.push({ username: req.body.username });
  console.log(users);
});
app.get("/users", (req, res, next) => {
  res.json(users);
});
app.listen(5000);
