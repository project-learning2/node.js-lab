import { Link } from "react-router-dom";
import React, { useState } from "react";

const Users = () => {
  const [dataUsers, setDataUsers] = useState();
  fetch("http://localhost:5000/users")
    .then((response) => response.json())
    .then((data) => setDataUsers(data))
    .catch((error) => console.error(error));

  const userRender = dataUsers ? (
    dataUsers.map((el) => {
      return <li key={el.username}>{el.username}</li>;
    })
  ) : (
    <p>Loaddinggg........</p>
  );
  return (
    <>
      <nav>
        <Link to="/">Add User</Link>
        <Link to="/users">Users</Link>
      </nav>
      <div>
        <h1>Users</h1>
        <ul>{userRender}</ul>
      </div>
    </>
  );
};

export default Users;
