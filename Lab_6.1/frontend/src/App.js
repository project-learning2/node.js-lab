import React from "react";
import { createBrowserRouter, RouterProvider } from "react-router-dom";

// import "./App.css";
import AddUser from "./AddUser";
import Users from "./Users";

const router = createBrowserRouter([
  {
    path: "/",
    element: <AddUser />,
  },
  {
    path: "/users",
    element: <Users />,
  },
]);

function App() {
  return <RouterProvider router={router}></RouterProvider>;
}

export default App;
