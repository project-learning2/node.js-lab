import { Link, useNavigate } from "react-router-dom";
import React, { useState } from "react";

const AddUser = () => {
  const [enteredUsername, setEnteredUsername] = useState("");
  const navigate = useNavigate();

  const changeUsernameHandle = (event) => {
    setEnteredUsername(event.target.value);
  };

  const submitAddUserHandle = (event) => {
    event.preventDefault();
    if (enteredUsername.trim().length === 0) {
      alert("wrong");
    } else {
      const dataUser = {
        username: enteredUsername,
      };
      fetch("http://localhost:5000/add-user", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(dataUser),
      })
        .then(navigate("/users"))
        .catch((error) => console.error(error));
      setEnteredUsername("");
    }
    // navigate("/users");
  };

  return (
    <>
      <nav>
        <Link to="/">Add User</Link>
        <Link to="/users">Users</Link>
      </nav>
      <form className="add-user-form" onSubmit={submitAddUserHandle}>
        <div className="form-control">
          <input
            type="text"
            name="username"
            id="username"
            value={enteredUsername}
            onChange={changeUsernameHandle}
          />
        </div>
        <button className="btn" type="submit">
          Add User
        </button>
      </form>
    </>
  );
};

export default AddUser;
