const Product = require("../models/product");

exports.getAddProduct = (req, res, next) => {
  res.render("add-product", {
    pageTitle: "Add Product",
    path: "/admin/add-product",
    formsCSS: true,
    productCSS: true,
    activeAddProduct: true,
  });
};

// // /admin/add-product => POST
exports.postAddProduct = (req, res, next) => {
  const id = req.body.id;
  const title = req.body.title;
  const price = Number(req.body.price);
  const imageUrl = req.body.imageUrl;
  const description = req.body.description;
  const product = new Product(id, title, price, imageUrl, description);
  console.log(typeof price);
  product.save();
  // console.log(product);
  res.status(200).json("sucess");
};

// /admin/edit-product => POST
exports.postEditProduct = (req, res, next) => {
  const id = req.body.id;
  const title = req.body.title;
  const price = req.body.price;
  const imageUrl = req.body.imageUrl;
  const description = req.body.description;
  Product.fetchAll((products) => {
    // Tìm Product trong danh sách
    const productIndex = products.findIndex((product) => product.id === id);
    if (productIndex === -1) {
      const message = "Some thing went wrong, this product not in database now";
      res.json(message);
    } else {
      const productEdit = new Product(id, title, price, imageUrl, description);
      productEdit.edit(productIndex);
      res.status(200).json("edit sucess");
    }
  });
};

exports.getProducts = (req, res, next) => {
  // Product.fetchAll((products) => {
  //   res.json(products);
  // });
  Product.fetchAll().then(([rows, feildData]) => res.json(rows));
};
