const fs = require("fs");
const path = require("path");

const db = require("../utils/database");

const p = path.join(
  path.dirname(process.mainModule.filename),
  "data",
  "products.json"
);

// const getProductsFromFile = (cb) => {
//   fs.readFile(p, (err, fileContent) => {
//     if (err) {
//       cb([]);
//     } else {
//       cb(JSON.parse(fileContent));
//     }
//   });
// };

module.exports = class Product {
  constructor(id, title, price, imageUrl, description) {
    this.id = id;
    this.title = title;
    this.price = price;
    this.imageUrl = imageUrl;
    this.description = description;
  }

  edit(index) {
    getProductsFromFile((products) => {
      products[index] = this;
      fs.writeFile(p, JSON.stringify(products), (err) => {
        console.log(err);
      });
    });
  }
  save() {
    getProductsFromFile((products) => {
      products.push(this);
      fs.writeFile(p, JSON.stringify(products), (err) => {
        console.log(err);
      });
    });
  }
  static findById(id) {
    return db.execute("SELECT * FROM products WHERE products.id = ?", [id]);
  }

  static fetchAll(cb) {
    // getProductsFromFile(cb);
    return db.execute("SELECT * FROM products");
  }
};
