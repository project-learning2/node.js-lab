import { createBrowserRouter, RouterProvider } from "react-router-dom";
import RootLayout from "./components/layouts/RootLayout";
import AddProduct from "./pages/AddProduct/AddProduct";
import AdminProduct from "./pages/AdminProducts/AdminProducts";
import CartPage from "./pages/CartPage/CartPage";
import ErrorPage from "./pages/ErrorPage";
import ShopPage from "./pages/ShopPage/ShopPage";

const router = createBrowserRouter([
  {
    path: "/",
    element: <RootLayout />,
    errorElement: <ErrorPage />,
    children: [
      { path: "/", element: <ShopPage /> },
      { path: "/add-product", element: <AddProduct /> },
      { path: "/cart-page", element: <CartPage /> },
      { path: "/admin-products", element: <AdminProduct /> },
    ],
  },
]);

function App() {
  return <RouterProvider router={router} />;
}

export default App;
