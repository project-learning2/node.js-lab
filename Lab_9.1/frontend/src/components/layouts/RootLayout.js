import { Outlet } from "react-router-dom";
import MainNavigation from "./MainNavigation";
import styles from "./RootLayout.module.css";

const RootLayout = () => {
  return (
    <div className={styles["root-layout"]}>
      <MainNavigation />
      <main className="">
        <Outlet />
      </main>
    </div>
  );
};

export default RootLayout;
