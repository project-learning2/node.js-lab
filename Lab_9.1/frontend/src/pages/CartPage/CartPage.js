import { useState } from "react";
import axios from "axios";

import styles from "./CartPage.module.css";

const CartPage = () => {
  const [cart, setCart] = useState(null);
  const [dataProducts, setDataProducts] = useState(null);

  //   Có thể giảm số lượng lần call api tất cả sản phầm

  const fetchAPI = () => {
    fetch("http://localhost:5000/get-cart")
      .then((response) => response.json())
      .then((data) => {
        setCart(data);
      })
      .catch((error) => console.error(error));
    fetch("http://localhost:5000/")
      .then((response) => response.json())
      .then((data) => {
        setDataProducts(data);
      })
      .catch((err) => console.error(err));
  };
  fetchAPI();

  const deleteHandler = (product) => {
    axios
      .post("http://localhost:5000/delete-product", product)
      .then((response) => {
        alert(response.data);
        fetchAPI();
      })
      .catch((err) => console.error(err));
  };

  const productsRender =
    cart && dataProducts && cart.products.length > 0
      ? cart.products.map((el) => {
          const product = dataProducts.find((product) => product.id === el.id);
          return (
            <div key={product.id} className={styles["product-el"]}>
              <h2>{product.title}</h2>
              <img src={product.imageUrl} alt="" />
              <h1>{product.price}$</h1>
              <p>{product.description}</p>
              <p>Qty: {el.qty}</p>
              <button onClick={() => deleteHandler(product)}>Delete</button>
            </div>
          );
        })
      : "0 product in cart";

  return (
    <div className={styles["cart-page"]}>
      <div className={styles.product}>{productsRender}</div>
      <h4>Total Price: {cart ? cart.totalPrice : "Loadingg......"}</h4>
    </div>
  );
};

export default CartPage;
