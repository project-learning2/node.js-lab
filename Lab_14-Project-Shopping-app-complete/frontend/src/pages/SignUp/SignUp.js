import axios from "axios";
import { useState } from "react";
import { useNavigate } from "react-router-dom";

import styles from "./SignUp.module.css";

const SignUp = () => {
  const navigate = useNavigate();
  const [enteredName, setEnteredName] = useState("");
  const [enteredEmail, setEnteredEmail] = useState("");
  const [enteredPassword, setEnteredPassword] = useState("");

  const changeNameHandle = (events) => {
    setEnteredName(events.target.value);
  };
  const changeEmailHanle = (events) => {
    setEnteredEmail(events.target.value);
  };
  const changePasswordHandle = (events) => {
    setEnteredPassword(events.target.value);
  };
  const submitHandle = (events) => {
    events.preventDefault();
    const formData = {
      name: enteredName,
      email: enteredEmail,
      password: enteredPassword,
    };
    axios
      .post("http://localhost:5000/signup-user", formData)
      .then((response) => {
        alert(response.data.message);
        navigate("/login");
      })
      .catch((err) => {
        console.log(err);
      });
  };

  return (
    <form className={styles.form} onSubmit={submitHandle}>
      <h2>Sign Up</h2>
      <div>
        <label>Name</label>
        <input
          type="text"
          placeholder="Enter Name"
          value={enteredName}
          onChange={changeNameHandle}
        />
      </div>
      <div>
        <label>Email</label>
        <input
          type="email"
          placeholder="Enter Email"
          value={enteredEmail}
          onChange={changeEmailHanle}
        />
      </div>
      <div>
        <label>Password</label>
        <input
          type="password"
          placeholder="Enter password"
          value={enteredPassword}
          onChange={changePasswordHandle}
        />
      </div>
      <button type="submit">Submit</button>
    </form>
  );
};

export default SignUp;
