import axios from "axios";
import { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";

import styles from "./ProductDetail.module.css";

const ProductDetail = () => {
  const navigate = useNavigate();
  const productId = useParams().prodId;
  const [productData, setProductData] = useState(null);

  useEffect(() => {
    axios
      .get(`http://localhost:5000/get-product/${productId}`)
      .then((response) => {
        setProductData(response.data);
      })
      .catch((err) => {
        console.log(err);
      });
  }, [productId]);

  return (
    <div className={styles["product-detail"]}>
      <button
        onClick={() => {
          navigate("/");
        }}
      >
        X
      </button>
      {productData && (
        <>
          <h3>{productData.title}</h3>
          <img alt="" src={productData.imageUrl} />
          <h5>{productData.price}$</h5>
          <p>{productData.description}</p>
        </>
      )}
    </div>
  );
};

export default ProductDetail;
