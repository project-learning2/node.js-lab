import axios from "axios";
import { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";

import styles from "./ShopPage.module.css";

const ShopPage = () => {
  const isAuth = useSelector((state) => state.auth.isAuthenticated);
  const userLogin = useSelector((state) => state.auth.userLogin);
  const [dataProducts, setDataProducts] = useState(null);
  const navigate = useNavigate();

  useEffect(() => {
    axios
      .get("http://localhost:5000/get-products")
      .then((response) => {
        setDataProducts(response.data);
      })
      .catch((err) => console.error(err));
  }, []);

  const addToCartHandle = (product) => {
    if (!isAuth) {
      alert("Login to add cart");
      navigate("/login");
    } else {
      axios
        .post("http://localhost:5000/add-to-cart", {
          productId: product._id,
          userId: userLogin.email,
        })
        .then((response) => {
          alert(response.data.message);
          navigate("/cart-page");
        })
        .catch((err) => console.error(err));
    }
  };
  const navProductDetailHandle = (prodId) => {
    navigate(`/product-detail/${prodId}`);
  };

  const productsRender =
    dataProducts && dataProducts.length > 0 ? (
      dataProducts.map((el, index) => {
        return (
          <div key={index + 1} className={styles["product-el"]}>
            <h2>{el.title}</h2>
            <img src={el.imageUrl} alt="" />
            <h1>{el.price}$</h1>
            <p>{el.description}</p>
            <button onClick={() => addToCartHandle(el)}>Add to Cart</button>
            <button onClick={() => navProductDetailHandle(el._id)}>
              Product Detail
            </button>
          </div>
        );
      })
    ) : (
      <p>0 product</p>
    );
  return <div className={styles["shop-page"]}>{productsRender}</div>;
};

export default ShopPage;
