import { useEffect, useState } from "react";
import axios from "axios";

import styles from "./CartPage.module.css";
import { useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";

const CartPage = () => {
  const userLogin = useSelector((state) => state.auth.userLogin);
  const [cart, setCart] = useState(null);
  const navigate = useNavigate();
  let totalPrice = 0;
  useEffect(() => {
    if (!userLogin) {
      navigate("/login");
    }
    axios
      .get(`http://localhost:5000/get-cart/${userLogin.email}`)
      .then((response) => {
        setCart(response.data);
      })
      .catch((error) => console.error(error));
  }, [userLogin]);

  const removeProductHandle = (prodId) => {
    axios
      .post(`http://localhost:5000/remove-product`, {
        productId: prodId,
        userEmail: userLogin.email,
      })
      .then((response) => {
        // console.log(response);
        alert(response.data.message);
        navigate("/");
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const cartRender =
    cart && cart.length > 0 ? (
      cart.map((el) => {
        totalPrice += el.productId.price * el.quantity;
        return (
          <div key={el._id} className={styles["product-el"]}>
            <h2>{el.productId.title}</h2>
            <img src={el.productId.imageUrl} alt="" />
            <h1>{el.productId.price}$</h1>
            <p>{el.productId.description}</p>
            <p>quantity: {el.quantity}</p>
            <button
              onClick={() => {
                removeProductHandle(el.productId._id);
              }}
            >
              Remove
            </button>
            <button
              onClick={() => {
                navigate(`/product-detail/${el.productId._id}`);
              }}
            >
              Product Detail
            </button>
          </div>
        );
      })
    ) : (
      <p>Loadinggg............</p>
    );

  return (
    <div className={styles["cart-page"]}>
      <button>Order this cart</button>
      <div className={styles.product}>
        {cart && cart.length === 0 ? "0 product in cart" : cartRender}
      </div>
      <h4>Total Price: {totalPrice}</h4>
    </div>
  );
};

export default CartPage;
