import axios from "axios";
import { useState } from "react";
import { useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";

import styles from "./Login.module.css";
import { authActions } from "../../store/auth";

const Login = () => {
  const navigate = useNavigate();
  const dispath = useDispatch();
  const [enteredEmail, setEnteredEmail] = useState("");
  const [enteredPassword, setEnteredPassword] = useState("");

  const changeEmailHanle = (events) => {
    setEnteredEmail(events.target.value);
  };
  const changePasswordHandle = (events) => {
    setEnteredPassword(events.target.value);
  };
  const submitHandle = (events) => {
    events.preventDefault();
    const formData = {
      email: enteredEmail,
      password: enteredPassword,
    };
    axios
      .post("http://localhost:5000/login", formData)
      .then((response) => {
        alert(response.data.message);
        dispath(authActions.login(formData));
        navigate("/");
      })
      .catch((err) => {
        console.log(err);
      });
  };

  return (
    <form className={styles.form} onSubmit={submitHandle}>
      <h2>Login</h2>
      <div>
        <label>Email</label>
        <input
          type="email"
          placeholder="Enter Email"
          value={enteredEmail}
          onChange={changeEmailHanle}
        />
      </div>
      <div>
        <label>Password</label>
        <input
          type="password"
          placeholder="Enter password"
          value={enteredPassword}
          onChange={changePasswordHandle}
        />
      </div>
      <button type="submit">Submit</button>
    </form>
  );
};

export default Login;
