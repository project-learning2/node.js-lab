import { createSlice } from "@reduxjs/toolkit";

const initialAuthStore = {
  userLogin: null,
  isAuthenticated: false,
};

const authSlice = createSlice({
  name: "authentication",
  initialState: initialAuthStore,
  reducers: {
    login(state, action) {
      state.isAuthenticated = true;
      state.userLogin = action.payload;
    },
    logout(state) {
      state.isAuthenticated = false;
      state.userLogin = null;
    },
  },
});

export const authActions = authSlice.actions;

export default authSlice.reducer;
