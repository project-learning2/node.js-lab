import { useSelector } from "react-redux";
import { NavLink, useNavigate } from "react-router-dom";

import styles from "./MainNavigation.module.css";

const MainNavigation = () => {
  const navigate = useNavigate();
  const isAuth = useSelector((state) => state.auth.isAuthenticated);
  // console.log(isAuth);

  return (
    <header className={styles.header}>
      <nav className={styles.nav}>
        <ul>
          <li>
            <NavLink
              className={({ isActive }) =>
                isActive ? styles.active : undefined
              }
              to="/"
            >
              Shop
            </NavLink>
          </li>
          <li>
            <NavLink
              className={({ isActive }) =>
                isActive ? styles.active : undefined
              }
              to="/pendding"
            >
              Products
            </NavLink>
          </li>
          <li>
            <NavLink
              className={({ isActive }) =>
                isActive ? styles.active : undefined
              }
              to="/cart-page"
            >
              Cart
            </NavLink>
          </li>
          <li>
            <NavLink
              className={({ isActive }) =>
                isActive ? styles.active : undefined
              }
              to="/pendding"
            >
              Orders
            </NavLink>
          </li>
          <li>
            <NavLink
              className={({ isActive }) =>
                isActive ? styles.active : undefined
              }
              to="/add-product"
            >
              Add Product
            </NavLink>
          </li>
          <li>
            <NavLink
              className={({ isActive }) =>
                isActive ? styles.active : undefined
              }
              to="/admin-products"
            >
              Admin Products
            </NavLink>
          </li>
        </ul>
        <ul className={styles["state-login"]}>
          {!isAuth && (
            <li>
              <button
                onClick={() => {
                  navigate("/signup");
                }}
              >
                Register
              </button>
              <button
                onClick={() => {
                  navigate("/login");
                }}
              >
                Login
              </button>
            </li>
          )}
          {/* <li></li> */}
        </ul>
      </nav>
    </header>
  );
};

export default MainNavigation;
