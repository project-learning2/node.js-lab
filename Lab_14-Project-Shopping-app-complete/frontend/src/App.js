import { createBrowserRouter, RouterProvider } from "react-router-dom";
import RootLayout from "./components/layouts/RootLayout";
import AddProduct from "./pages/AddProduct/AddProduct";
import AdminProduct from "./pages/AdminProducts/AdminProducts";
import CartPage from "./pages/CartPage/CartPage";
import ErrorPage from "./pages/ErrorPage";
import Login from "./pages/Login/Login";
import PenddingPage from "./pages/PenddingPage/PenddingPage";
import ProductDetail from "./pages/ProductDetail/ProductDetail";
import ShopPage from "./pages/ShopPage/ShopPage";
import SignUp from "./pages/SignUp/SignUp";

const router = createBrowserRouter([
  {
    path: "/",
    element: <RootLayout />,
    errorElement: <ErrorPage />,
    children: [
      {
        path: "/",
        element: <ShopPage />,
      },
      {
        path: "/product-detail/:prodId",
        element: <ProductDetail />,
      },
      {
        path: "/signup",
        element: <SignUp />,
      },
      {
        path: "login",
        element: <Login />,
      },
      { path: "/add-product", element: <AddProduct /> },
      { path: "/cart-page", element: <CartPage /> },
      { path: "/admin-products", element: <AdminProduct /> },
      { path: "/pendding", element: <PenddingPage /> },
      // { path: "/products-page", element: <ShopPage /> },
    ],
  },
]);

function App() {
  return <RouterProvider router={router} />;
}

export default App;
