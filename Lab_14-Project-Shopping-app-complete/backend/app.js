const express = require("express");
const bodyParse = require("body-parser");
const cors = require("cors");
const mongoose = require("mongoose");

const app = express();

const adminRoutes = require("./routes/admin");
const shopRoutes = require("./routes/shop");

app.use(cors());
app.use(bodyParse.json());

app.use("/", shopRoutes);
app.use("/admin", adminRoutes);

mongoose
  .connect(
    `mongodb+srv://${process.env.MONGO_USER}:${process.env.MONGO_PASSWORD}@cluster0.jyxohlo.mongodb.net/${process.env.MONGO_DATABASE}`
  )
  .then(() => {
    console.log("Connected MongoDB success!!");
    app.listen(5000);
  })
  .catch((err) => {
    console.log(err);
  });
