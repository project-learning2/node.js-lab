const User = require("../models/User");

exports.postSignUp = (req, res, next) => {
  const name = req.body.name;
  const email = req.body.email;
  const password = req.body.password;

  const newUser = new User({
    name: name,
    email: email,
    password: password,
  });

  newUser
    .save()
    .then(() => {
      res.status(201).json({ message: "Create new user success!!" });
    })
    .catch((err) => {
      console.log(err);
      res.status(500).json({ message: "Some thing went wrong!!" });
    });
};

exports.postLogin = (req, res, next) => {
  const email = req.body.email;
  const password = req.body.password;

  User.findOne({ email: email })
    .then((user) => {
      if (user) {
        if (user.password === password) {
          res.status(200).json({ message: "Login success!!" });
        }
      } else {
        res.status(404).json({ message: "email or password is correct!!" });
      }
    })
    .catch((err) => {
      res.end();
    });
};
