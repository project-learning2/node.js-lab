const Product = require("../models/product");
const Order = require("../models/order");
const User = require("../models/User");

exports.getProducts = (req, res, next) => {
  Product.find()
    .then((products) => {
      res.status(200).json(products);
    })
    .catch((err) => {
      console.log(err);
    });
};

exports.getProduct = (req, res, next) => {
  const prodId = req.params.productId;
  Product.findById(prodId)
    .then((product) => {
      res.status(200).json(product);
    })
    .catch((err) => console.log(err));
};

exports.getIndex = (req, res, next) => {
  Product.find()
    .then((products) => {
      res.render("shop/index", {
        prods: products,
        pageTitle: "Shop",
        path: "/",
      });
    })
    .catch((err) => {
      console.log(err);
    });
};

exports.getCart = (req, res, next) => {
  const userEmail = req.params.userEmail;
  User.findOne({ email: userEmail })
    .populate("cart.items.productId")
    .exec()
    .then((user) => {
      res.status(200).json(user.cart.items);
    })
    .catch((err) => {
      console.log(err);
    });
};

exports.postCart = (req, res, next) => {
  const prodId = req.body.productId;
  const userEmail = req.body.userId;
  Product.findById(prodId)
    .then((product) => {
      if (product) {
        User.findOne({ email: userEmail }).then((user) => {
          user.addToCart(product);
          res.status(200).json({ message: "Add To Cart Success!!" });
        });
      }
    })
    .catch((err) => {
      console.log(err);
      res.status(500).json({ message: "Some thing went wrong!!" });
    });
};

exports.postCartDeleteProduct = (req, res, next) => {
  const prodId = req.body.productId;
  const userEmail = req.body.userEmail;
  User.findOne({ email: userEmail })
    .then((user) => {
      user
        .removeFromCart(prodId)
        .then(() => {
          res.status(200).json({ message: "remove success" });
        })
        .catch((err) => console.log(err));
    })
    .catch((err) => console.log(err));
};

exports.postOrder = (req, res, next) => {
  req.user
    .populate("cart.items.productId")
    .execPopulate()
    .then((user) => {
      const products = user.cart.items.map((i) => {
        return { quantity: i.quantity, product: { ...i.productId._doc } };
      });
      const order = new Order({
        user: {
          name: req.user.name,
          userId: req.user,
        },
        products: products,
      });
      return order.save();
    })
    .then((result) => {
      return req.user.clearCart();
    })
    .then(() => {
      res.redirect("/orders");
    })
    .catch((err) => console.log(err));
};

exports.getOrders = (req, res, next) => {
  Order.find({ "user.userId": req.user._id })
    .then((orders) => {
      res.render("shop/orders", {
        path: "/orders",
        pageTitle: "Your Orders",
        orders: orders,
      });
    })
    .catch((err) => console.log(err));
};
