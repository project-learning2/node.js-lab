const express = require("express");

const router = express.Router();

const authController = require("../controllers/auth");
const shopController = require("../controllers/shop");

router.post("/signup-user", authController.postSignUp);
router.post("/login", authController.postLogin);
router.get("/get-products", shopController.getProducts);
router.get("/get-product/:productId", shopController.getProduct);
router.post("/add-to-cart", shopController.postCart);
router.get("/get-cart/:userEmail", shopController.getCart);
router.post("/remove-product", shopController.postCartDeleteProduct);

module.exports = router;
