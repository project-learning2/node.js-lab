const CartItem = require("../models/cart");
const Products = require("../models/product");

//  /add-cart => POST
exports.postAddProductToCart = (req, res, next) => {
  const id = req.body.id;
  CartItem.findByPk(id)
    .then((result) => {
      if (result === null) {
        CartItem.create({
          id: id,
          quantity: 1,
        })
          .then(() => {
            res.status(200).json("add to cart sucess");
          })
          .catch((err) => {
            console.log(err);
          });
      } else {
        CartItem.increment("quantity", { by: 1, where: { id: result.id } });
        res.status(200).json("add to cart sucess");
      }
    })
    .catch((err) => console.log(err));
};

//  http://localhost:5000/get-cart
exports.getCart = (req, res, next) => {
  const itemInCart = [];
  const productInCart = [];
  CartItem.findAll()
    .then((result) => {
      result.forEach((item) => itemInCart.push(item.get()));
      for (let i = 0; i < itemInCart.length; i++) {
        Products.findByPk(itemInCart[i].id)
          .then((result) => result.get())
          .then((product) => {
            product.quantity = itemInCart[i].quantity;
            productInCart.push(product);
            if (i + 1 === itemInCart.length) {
              // console.log(productInCart);
              res.status(200).json(productInCart);
            }
          })
          .catch((err) => console.log(err));
      }
    })
    // .then(res.status(200).json(productInCart))
    .catch((err) => console.log(err));
};

exports.deleteProduct = (req, res, next) => {
  Cart.deleteProduct(req.body.id, req.body.price);
  res.status(200).json("Delete complete!!");
};
