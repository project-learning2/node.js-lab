const Product = require("../models/product");

exports.getAddProduct = (req, res, next) => {};

// // /admin/add-product => POST
exports.postAddProduct = (req, res, next) => {
  const id = req.body.id;
  const title = req.body.title;
  const imageUrl = req.body.imageUrl;
  const price = req.body.price;
  const description = req.body.description;
  Product.create({
    id: id,
    title: title,
    price: price,
    imageUrl: imageUrl,
    description: description,
  })
    .then((result) => {
      // console.log("Create Product", result);
      res.json("Create Product");
    })
    .catch((err) => {
      console.log(err);
    });
};

// /admin/edit-product => POST
exports.postEditProduct = (req, res, next) => {};

exports.getProducts = (req, res, next) => {
  Product.findAll()
    .then((products) => {
      res.json(products);
    })
    .catch((err) => console.error(err));
};
