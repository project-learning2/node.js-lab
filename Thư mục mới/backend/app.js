const express = require("express");
const bodyParser = require("body-parser");
const path = require("path");

const cors = require("cors");

const app = express();
app.use(cors());

const adminRoutes = require("./routes/admin");
const shopRoutes = require("./routes/shop");

const sequelize = require("./utils/database");

app.use(bodyParser.json());
// app.use(express.static(path.join(__dirname, "public")));

app.use("/admin", adminRoutes);
app.use(shopRoutes);

sequelize
  .sync()
  .then((result) => {
    app.listen(5000);
  })
  .catch((err) => console.error(err));
