import { useEffect, useState } from "react";
import axios from "axios";

import styles from "./CartPage.module.css";

const CartPage = () => {
  const [cart, setCart] = useState(null);
  let totalPrice = 0;
  useEffect(() => {
    axios
      .get("http://localhost:5000/get-cart")
      .then((response) => response.data)
      .then((data) => {
        setCart(data);
      })
      .catch((error) => console.error(error));
  }, []);

  const cartRender =
    cart && cart.length > 0 ? (
      cart.map((el) => {
        totalPrice += el.price * el.quantity;
        return (
          <div key={el.id} className={styles["product-el"]}>
            <h2>{el.title}</h2>
            <img src={el.imageUrl} alt="" />
            <h1>{el.price}$</h1>
            <p>{el.description}</p>
            <button>Remove</button>
            <button>Product Detail</button>
          </div>
        );
      })
    ) : (
      <p>Loadinggg............</p>
    );

  return (
    <div className={styles["cart-page"]}>
      <div className={styles.product}>
        {cart && cart.length === 0 ? "0 product in cart" : cartRender}
      </div>
      <h4>Total Price: {totalPrice}</h4>
    </div>
  );
};

export default CartPage;
