import { createBrowserRouter, RouterProvider } from "react-router-dom";
import RootLayout from "./components/layouts/RootLayout";
import AddProduct from "./pages/AddProduct/AddProduct";
import AdminProduct from "./pages/AdminProducts/AdminProducts";
import CartPage from "./pages/CartPage/CartPage";
import ErrorPage from "./pages/ErrorPage";
import PenddingPage from "./pages/PenddingPage/PenddingPage";
import ProductDetail from "./pages/ProductDetail/ProductDetail";
import ShopPage from "./pages/ShopPage/ShopPage";

const router = createBrowserRouter([
  {
    path: "/",
    element: <RootLayout />,
    errorElement: <ErrorPage />,
    children: [
      {
        path: "/",
        element: <ShopPage />,
      },
      {
        path: "/product-detail/:prodId",
        element: <ProductDetail />,
      },
      // {
      //   path: "/products-page",
      //   element: <ShopPage />,
      //   children: [
      //     {
      //       path: "/product-detail/:prodId",
      //       element: <ProductDetail />,
      //     },
      //   ],
      // },
      { path: "/add-product", element: <AddProduct /> },
      { path: "/cart-page", element: <CartPage /> },
      { path: "/admin-products", element: <AdminProduct /> },
      { path: "/pendding", element: <PenddingPage /> },
      // { path: "/products-page", element: <ShopPage /> },
    ],
  },
]);

function App() {
  return <RouterProvider router={router} />;
}

export default App;
