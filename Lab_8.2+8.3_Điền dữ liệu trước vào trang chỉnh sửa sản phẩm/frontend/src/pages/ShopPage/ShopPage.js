import axios from "axios";
import { useEffect, useState } from "react";

import styles from "./ShopPage.module.css";

const ShopPage = () => {
  const [dataProducts, setDataProducts] = useState(null);

  useEffect(() => {
    fetch("http://localhost:5000/")
      .then((response) => response.json())
      .then((data) => setDataProducts(data))
      .catch((err) => console.error(err));
  }, []);

  const addToCartHandle = (product) => {
    axios
      .post("http://localhost:5000/add-to-cart", product)
      .then((response) => {
        alert(response.data);
      })
      .catch((err) => console.error(err));
  };

  const productsRender = dataProducts ? (
    dataProducts.map((el, index) => {
      return (
        <div key={index + 1} className={styles["product-el"]}>
          <h2>{el.title}</h2>
          <img src={el.imageUrl} alt="" />
          <h1>{el.price}$</h1>
          <p>{el.description}</p>
          <button onClick={() => addToCartHandle(el)}>Add to Cart</button>
        </div>
      );
    })
  ) : (
    <p>Loadingg........</p>
  );
  return <div className={styles["shop-page"]}>{productsRender}</div>;
};

export default ShopPage;
