const path = require("path");

const express = require("express");

const productsController = require("../controllers/products");
const cartController = require("../controllers/cart");

const router = express.Router();

router.get("/", productsController.getProducts);
router.get("/get-cart", cartController.getCart);
router.post("/add-to-cart", cartController.postAddProductToCart);

module.exports = router;
