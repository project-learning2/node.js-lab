const Cart = require("../models/cart");

exports.postAddProductToCart = (req, res, next) => {
  const id = req.body.id;
  const price = req.body.price;
  Cart.addToCart(id, price);
  res.status(200).json("add to cart sucess");
};

exports.getCart = (req, res, next) => {
  Cart.fetchAll((cart) => res.json(cart));
};
