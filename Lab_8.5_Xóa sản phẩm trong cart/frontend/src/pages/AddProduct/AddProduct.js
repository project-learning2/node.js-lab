import { useState } from "react";
import axios from "axios";
import { useNavigate } from "react-router-dom";
import styles from "./AddProduct.module.css";

const AddProduct = () => {
  const [enteredTitle, setEnteredTitle] = useState("");
  const [enteredPrice, setEnteredPrice] = useState("");
  const [enteredImageUrl, setEnteredImageUrl] = useState("");
  const [enteredDescription, setEnteredDescription] = useState("");
  const navigate = useNavigate();

  const onChangeTitleHandle = (event) => {
    setEnteredTitle(event.target.value);
  };
  const onChangePriceHandle = (event) => {
    setEnteredPrice(event.target.value);
  };
  const onChangeImageUrlHandle = (event) => {
    setEnteredImageUrl(event.target.value);
  };
  const onChangeDescriptionHandle = (event) => {
    setEnteredDescription(event.target.value);
  };

  const submitHandle = (event) => {
    event.preventDefault();
    const id = Math.floor(Math.random() * 1000 + 1).toString();
    axios
      .post("http://localhost:5000/admin/add-product", {
        id: id,
        title: enteredTitle,
        price: enteredPrice,
        imageUrl: enteredImageUrl,
        description: enteredDescription,
      })
      .then((response) => {
        alert(response.data);
      })
      .then(() => {
        navigate("/");
      })
      .catch((error) => {
        console.error(error);
      });
  };
  return (
    <form className={styles["add-product"]} onSubmit={submitHandle}>
      <div>
        <label>Title</label>
        <input
          id="title"
          name="title"
          type="text"
          value={enteredTitle}
          onChange={onChangeTitleHandle}
        />
      </div>
      <div>
        <label>URL Image</label>
        <input
          id="imageUrl"
          name="imageUrl"
          type="text"
          value={enteredImageUrl}
          onChange={onChangeImageUrlHandle}
        />
      </div>
      <div>
        <label>Price</label>
        <input
          id="price"
          name="price"
          type="number"
          value={enteredPrice}
          onChange={onChangePriceHandle}
        />
      </div>
      <div className={styles.description}>
        <label>Description</label>
        <input
          value={enteredDescription}
          id="description"
          name="description"
          type="text"
          onChange={onChangeDescriptionHandle}
        />
      </div>
      <button type="submit">Add Product</button>
    </form>
  );
};

export default AddProduct;
