const fs = require("fs");
const path = require("path");

const p = path.join(
  path.dirname(process.mainModule.filename),
  "data",
  "cart.json"
);

const getCartFromFile = (cb) => {
  fs.readFile(p, (err, fileContent) => {
    if (err) {
      cb({
        products: [],
        totalPrice: 0,
      });
    } else {
      cb(JSON.parse(fileContent));
    }
  });
};

module.exports = class Cart {
  constructor(products, totalPrice) {
    this.products = products;
    this.totalPrice = totalPrice;
  }
  static deleteProduct(productId, productPrice) {
    getCartFromFile((cart) => {
      const products = cart.products;
      const productIndex = products.findIndex((el) => el.id === productId);
      if (productIndex === -1) {
        console.log("some thing went wrong");
      } else {
        if (products[productIndex].qty === 1) {
          // trong cart có 1 sản phầm
          products.splice(productIndex, 1);
          cart.totalPrice -= productPrice;
        } else {
          //  trong cart còn nhiều sản phẩm
          products[productIndex].qty--;
          console.log(cart);
          cart.totalPrice -= productPrice;
        }
        fs.writeFile(p, JSON.stringify(cart), (err) => console.log(err));
      }
    });
  }
  static addToCart(productId, productPrice) {
    getCartFromFile((cart) => {
      const productsInCart = cart.products;
      const productIdIndex = productsInCart.findIndex(
        (product) => productId === product.id
      );
      if (productIdIndex === -1) {
        // product không có sẵn trong cart

        // thêm sản phẩm đó vào giỏ hàng với số lượng là 1
        productsInCart.push({
          // có được push??????????????????????????????????????
          id: productId,
          qty: 1,
        });
        // chỉnh sửa lại totalPrice trong giỏ
        cart.totalPrice += productPrice;
      } else {
        //   Nếu product đã có sẵn trong
        // tăng qty lên 1 đơn vị
        productsInCart[productIdIndex].qty++;
        // cập nhập lại totalPrice
        cart.totalPrice += productPrice;
      }
      //   Save lại cart
      fs.writeFile(p, JSON.stringify(cart), (err) => console.log(err));
    });
  }
  static fetchAll(cb) {
    getCartFromFile(cb);
  }
};
