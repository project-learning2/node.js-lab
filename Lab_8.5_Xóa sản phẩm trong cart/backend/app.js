const express = require("express");
const bodyParser = require("body-parser");
const path = require("path");

// const errorController = require("./controllers/error");
const cors = require("cors");

const app = express();
app.use(cors());

const adminRoutes = require("./routes/admin");
const shopRoutes = require("./routes/shop");

// app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(express.static(path.join(__dirname, "public")));

app.use("/admin", adminRoutes);
app.use(shopRoutes);

// app.use(errorController.get404);

app.listen(5000);
