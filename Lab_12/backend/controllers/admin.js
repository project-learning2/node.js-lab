const Product = require("../models/product");

exports.postAddProduct = (req, res, next) => {
  const title = req.body.title;
  const imageUrl = req.body.imageUrl;
  const price = req.body.price;
  const description = req.body.description;
  const product = new Product(title, price, description, imageUrl);
  product
    .save()
    .then((result) => {
      console.log("Created Product");
      res.status(200).json("Created Product");
    })
    .catch((err) => {
      console.log(err);
    });
};

exports.postEditProduct = (req, res, next) => {
  const id = req.body._id;
  const title = req.body.title;
  const price = req.body.price;
  const description = req.body.description;
  const imageUrl = req.body.imageUrl;

  Product.editProduct(id, title, price, description, imageUrl);
  res.status(200).json("edit successfully!!");
};

exports.postDeleteProduct = (req, res, next) => {
  const id = req.body._id;
  Product.deleteProduct(id);
  res.status(200).json("delete product successfully");
};
