const Cart = require("../models/cart");

exports.postAddToCart = (req, res, next) => {
  const id = req.body._id;
  const title = req.body.title;
  const price = req.body.price;
  const description = req.body.description;
  const imageUrl = req.body.imageUrl;

  Cart.findById(id).then((product) => {
    if (!product) {
      const cart = new Cart(id, title, price, description, imageUrl, 1);
      cart.save();
    } else {
      console.log("increment quantity successfully!!");
      const quanityUpdate = product.quantity + 1;
      Cart.incrementQuantity(id, quanityUpdate);
    }
  });
  res.status(200).json("addToCartSuccessfully");
};

exports.getCart = (req, res, next) => {
  Cart.fetchAll().then((productsInCart) => {
    res.status(200).json(productsInCart);
  });
};

exports.deleteProduct = (req, res, next) => {
  const id = req.body._id;
  Cart.deleteItem(id);
  res.status(200).json("delete item succesfully");
};
