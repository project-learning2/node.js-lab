const Product = require("../models/product");

exports.getProducts = (req, res, next) => {
  Product.fetchAll()
    .then((producs) => {
      res.status(200).json(producs);
    })
    .catch((err) => {
      console.log(err);
    });
};
