const express = require("express");

const shopController = require("../controllers/shop");
const cartController = require("../controllers/cart");

const router = express.Router();

router.get("/", shopController.getProducts);
router.get("/get-cart", cartController.getCart);
router.post("/add-to-cart", cartController.postAddToCart);
router.post("/delete-product", cartController.deleteProduct);

module.exports = router;
