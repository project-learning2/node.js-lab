const mongodb = require("mongodb");
const MongoClient = mongodb.MongoClient;

const url =
  "mongodb+srv://locnguyenddbrvt:Aas123123@cluster0.jyxohlo.mongodb.net/?retryWrites=true&w=majority";

const dbName = "demoNodeApp";

let _db;

const mongoConnect = (cb) => {
  MongoClient.connect(url)
    .then((client) => {
      console.log("Conected demo mongodb database successfully");
      _db = client.db(dbName);
      // console.log(_db);
      cb();
    })
    .catch((err) => {
      console.log(err);
      throw err;
    });
};

const getDb = () => {
  if (_db) {
    return _db;
  }
  throw "No database found";
};

exports.mongoConnect = mongoConnect;
exports.getDb = getDb;
