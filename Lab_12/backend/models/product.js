const getDb = require("../utils/database").getDb;
const mogodb = require("mongodb");
const ObjectId = mogodb.ObjectId;

class Product {
  constructor(title, price, description, imageUrl) {
    this.title = title;
    this.price = price;
    this.description = description;
    this.imageUrl = imageUrl;
  }
  save() {
    const db = getDb();
    return db
      .collection("products")
      .insertOne(this)
      .then((result) => {
        console.log(result);
      })
      .catch((err) => {
        console.log(err);
      });
  }

  static deleteProduct(id) {
    const db = getDb();
    db.collection("products")
      .deleteOne({ _id: new ObjectId(id) })
      .then(() => console.log("delete this product successfully"))
      .catch((err) => console.log(err));
  }

  static editProduct(id, title, price, description, imageUrl) {
    const db = getDb();
    db.collection("products")
      .updateOne(
        { _id: new ObjectId(id) },
        {
          $set: {
            title: title,
            price: price,
            description: description,
            imageUrl: imageUrl,
          },
        }
      )
      .then(() => console.log("editProductSucessfully"))
      .catch((err) => console.log(err));
  }

  static fetchAll() {
    const db = getDb();
    return db
      .collection("products")
      .find()
      .toArray()
      .then((products) => {
        // console.log(products);
        return products;
      })
      .catch((err) => {
        console.log(err);
      });
  }
}

module.exports = Product;
