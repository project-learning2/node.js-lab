const getDb = require("../utils/database").getDb;

class Cart {
  constructor(
    productId,
    productTitle,
    productPrice,
    productDescription,
    productImageUrl,
    quantity
  ) {
    this._id = productId;
    this.title = productTitle;
    this.price = productPrice;
    this.description = productDescription;
    this.imageUrl = productImageUrl;
    this.quantity = quantity;
  }
  static findById(id) {
    const db = getDb();
    return db
      .collection("cart")
      .findOne({ _id: id })
      .then((result) => result)
      .catch((err) => {
        console.log(err);
      });
  }

  static incrementQuantity(id, quantity) {
    const db = getDb();
    db.collection("cart")
      .updateOne({ _id: id }, { $set: { quantity: quantity } })
      .then(() => {
        console.log("increment successfully");
      })
      .catch((err) => {
        console.log(err);
      });
  }
  static deleteItem(id) {
    const db = getDb();
    db.collection("cart")
      .deleteOne({ _id: id })
      .then(() => console.log("delete this item successfully"))
      .catch((err) => console.log(err));
  }

  save() {
    const db = getDb();
    return db
      .collection("cart")
      .insertOne(this)
      .then((result) => {
        console.log("add this product to cart successfully", result);
      })
      .catch((err) => {
        console.log(err);
      });
  }

  static fetchAll() {
    const db = getDb();
    return db
      .collection("cart")
      .find()
      .toArray()
      .then((products) => {
        return products;
      })
      .catch((err) => {
        console.log(err);
      });
  }
}

module.exports = Cart;
