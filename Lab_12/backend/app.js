const express = require("express");
const bodyParser = require("body-parser");

const cors = require("cors");

const app = express();
app.use(cors());

const adminRoutes = require("./routes/admin");
const shopRoutes = require("./routes/shop");

const mongoConnect = require("./utils/database").mongoConnect;

app.use(bodyParser.json());

app.use("/admin", adminRoutes);
app.use(shopRoutes);

mongoConnect(() => {
  app.listen(5000);
});
