import axios from "axios";
import { useEffect, useState } from "react";
// import { useNavigate } from "react-router-dom";

import styles from "./AdminProducts.module.css";

const AdminProduct = () => {
  const [dataProducts, setDataProducts] = useState(null);
  const [editMode, setEditMode] = useState(false);
  const [idProduct, setIdProduct] = useState("");
  const [enteredTitle, setEnteredTitle] = useState("");
  const [enteredPrice, setEnteredPrice] = useState("");
  const [enteredImageUrl, setEnteredImageUrl] = useState("");
  const [enteredDescription, setEnteredDescription] = useState("");

  const fetchData = () => {
    fetch("http://localhost:5000/")
      .then((response) => response.json())
      .then((data) => setDataProducts(data))
      .catch((err) => console.error(err));
  };
  useEffect(() => {
    fetchData();
  }, []);

  const editProductHandle = (product) => {
    console.log(product);
    setEditMode(true);
    setIdProduct(product._id);
    setEnteredTitle(product.title);
    setEnteredImageUrl(product.imageUrl);
    setEnteredPrice(product.price);
    setEnteredDescription(product.description);
  };

  const closeEditModeHandle = () => {
    setEditMode(false);
  };
  const onChangeTitleHandle = (event) => {
    setEnteredTitle(event.target.value);
  };
  const onChangePriceHandle = (event) => {
    setEnteredPrice(event.target.value);
  };
  const onChangeImageUrlHandle = (event) => {
    setEnteredImageUrl(event.target.value);
  };
  const onChangeDescriptionHandle = (event) => {
    setEnteredDescription(event.target.value);
  };
  const submitHandle = (event) => {
    event.preventDefault();
    axios
      .post("http://localhost:5000/admin/edit-product", {
        _id: idProduct,
        title: enteredTitle,
        price: enteredPrice,
        description: enteredDescription,
        imageUrl: enteredImageUrl,
      })
      .then((response) => {
        fetchData();
        setEditMode(false);
        alert(response.data);
      })
      .catch((err) => console.error(err));
  };

  const deleteProductHandle = (product) => {
    axios
      .post("http://localhost:5000/admin/delete-product", product)
      .then((response) => {
        fetchData();
        alert(response.data);
      })
      .catch((err) => console.log(err));
  };
  const editModeDisplay = (
    <form className={styles["add-product"]} onSubmit={submitHandle}>
      <button onClick={closeEditModeHandle}>X</button>
      <div>
        <label>Title</label>
        <input
          id="title"
          name="title"
          type="text"
          value={enteredTitle}
          onChange={onChangeTitleHandle}
        />
      </div>
      <div>
        <label>URL Image</label>
        <input
          id="imageUrl"
          name="imageUrl"
          type="text"
          value={enteredImageUrl}
          onChange={onChangeImageUrlHandle}
        />
      </div>
      <div>
        <label>Price</label>
        <input
          id="price"
          name="price"
          type="number"
          value={enteredPrice}
          onChange={onChangePriceHandle}
        />
      </div>
      <div className={styles.description}>
        <label>Description</label>
        <input
          value={enteredDescription}
          id="description"
          name="description"
          type="text"
          onChange={onChangeDescriptionHandle}
        />
      </div>
      <button type="submit">Edit Product</button>
    </form>
  );

  const productsRender = dataProducts ? (
    dataProducts.map((el, index) => {
      return (
        <div key={index + 1} className={styles["product-el"]}>
          <h2>{el.title}</h2>
          <img src={el.imageUrl} alt="" />
          <h1>{el.price}$</h1>
          <p>{el.description}</p>
          <div className={styles.actions}>
            <button onClick={() => editProductHandle(el)}>Edit</button>
            <button onClick={() => deleteProductHandle(el)}>Delete</button>
          </div>
        </div>
      );
    })
  ) : (
    <p>Loadingg........</p>
  );
  return (
    <div className={styles["shop-page"]}>
      {!editMode ? productsRender : editModeDisplay}
    </div>
  );
};

export default AdminProduct;

//  Call Api về sever kiểm tra sản phẩm đó còn tồn tại trên database
