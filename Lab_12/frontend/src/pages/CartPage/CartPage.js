import { useEffect, useState } from "react";
import axios from "axios";
import { useNavigate } from "react-router-dom";

import styles from "./CartPage.module.css";

const CartPage = () => {
  const [cart, setCart] = useState(null);
  const navgigate = useNavigate();
  let totalPrice = 0;

  const fetchCart = () => {
    axios
      .get("http://localhost:5000/get-cart")
      .then((response) => response.data)
      .then((data) => {
        setCart(data);
      })
      .catch((error) => console.error(error));
  };
  useEffect(() => {
    fetchCart();
  }, []);
  const navProductDetailHanler = (id) => {
    navgigate(`/product-detail/:${id}`);
  };
  const removeProductInCartHandle = (product) => {
    axios
      .post("http://localhost:5000/delete-product", product)
      .then((response) => {
        alert(response.data);
        fetchCart();
      })
      .catch((err) => console.log(err));
  };

  const cartRender =
    cart && cart.length > 0 ? (
      cart.map((el) => {
        totalPrice += el.price * el.quantity;
        return (
          <div key={el._id} className={styles["product-el"]}>
            <h2>{el.title}</h2>
            <img src={el.imageUrl} alt="" />
            <h1>{el.price}$</h1>
            <p>{el.description}</p>
            <p>quantity: {el.quantity}</p>
            <button onClick={() => removeProductInCartHandle(el)}>
              Remove
            </button>
            <button onClick={() => navProductDetailHanler(el._id)}>
              Product Detail
            </button>
          </div>
        );
      })
    ) : (
      <p>Loadinggg............</p>
    );

  return (
    <div className={styles["cart-page"]}>
      <div className={styles.product}>
        {cart && cart.length === 0 ? "0 product in cart" : cartRender}
      </div>
      <h4>Total Price: {totalPrice}</h4>
    </div>
  );
};

export default CartPage;
