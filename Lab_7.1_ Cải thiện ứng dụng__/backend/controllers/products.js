const Product = require("../models/product");

exports.getAddProduct = (req, res, next) => {
  res.render("add-product", {
    pageTitle: "Add Product",
    path: "/admin/add-product",
    formsCSS: true,
    productCSS: true,
    activeAddProduct: true,
  });
};

// // /admin/add-product => POST
exports.postAddProduct = (req, res, next) => {
  // console.log(req.body);
  const title = req.body.title;
  const price = req.body.price;
  const imageUrl = req.body.imageUrl;
  const description = req.body.description;
  const product = new Product(title, price, imageUrl, description);
  product.save();
  console.log(product);
  // res.redirect("http://localhost:3000/");
  res.status.json("sucess");
};

exports.getProducts = (req, res, next) => {
  Product.fetchAll((products) => {
    res.json(products);
  });
};
