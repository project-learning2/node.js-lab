const path = require("path");

const express = require("express");

const app = express();

app.get("/", (req, res, next) => {
  res.sendFile(path.join(__dirname, "views", "index.html"));
});
app.get("/users", (req, res, next) => {
  res.sendFile(path.join(__dirname, "views", "users.html"));
});
app.use(
  express.static(path.join(__dirname, "public"), {
    // Thêm options để chỉ định kiểu MIME của file script
    setHeaders: (res, path) => {
      if (path.endsWith(".js")) {
        res.setHeader("Content-Type", "text/javascript");
      }
    },
  })
);

app.listen(3000);
// không đọc được file script????
